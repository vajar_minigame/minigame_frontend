syntax = "proto3";

import "common.proto";
import "google/api/annotations.proto";
package item;

option go_package = "gitlab.com/vajar_minigame/minigame_proto/go/item";

message Inventory {
  common.UserId id = 1;
  repeated Item items = 2;
}

message ItemList { repeated Item items = 1; }

message ItemDefList { repeated ItemDefinition itemDefs = 1; }
message ItemDefinition {
  string type = 2;
  repeated string tags = 3;
  oneof itemContent { Food food = 4; }
}

message Item {
  int32 id = 1;
  ItemDefinition itemDef = 2;
}

message Effect {
  oneof effect {
    Buff buff = 1;
    Recovery rec = 2;
  }
}

// effect types
// temporary increase of sth.
message Buff {
  string type = 1;
  float value = 2;
}

// recovery of resource
message Recovery {
  string type = 1;
  float value = 2;
}
message Page {
  int32 size = 1;
  int32 count = 2;
}

message Food { repeated Effect effects = 1; }
message ItemType { string type = 1; }
service ItemService {
  rpc CreateItemByType(ItemType) returns (common.ItemId) {
    option (google.api.http) = {
      post : "/v1/itemByType"
      body : "*"
    };
  }

  rpc AddItem(Item) returns (common.ItemId) {
    option (google.api.http) = {
      post : "/v1/item"
      body : "*"
    };
  }

  rpc GetItem(common.ItemId) returns (Item) {
    option (google.api.http) = {
      get : "/v1/item/{id}"
    };
  }
  rpc GetInventory(common.UserId) returns (Inventory) {
    option (google.api.http) = {
      get : "/v1/user/inventory/{id}"
    };
  }
  rpc UpdateItem(Item) returns (common.ResponseStatus) {
    option (google.api.http) = {
      put : "/v1/item"
      body : "*"
    };
  }

  rpc DeleteItem(common.ItemId) returns (common.ResponseStatus) {
    option (google.api.http) = {
      delete : "/v1/item/{id}"
    };
  }

  rpc GetItemDefinitions(Page) returns (ItemDefList) {}
}

//////////////////////////////////////////////////////////////////

message AddItem { Item item = 1; }
message GetItem { common.ItemId id = 1; }
message GetInventory { common.UserId userId = 1; }
message UpdateItem { Item item = 1; }
message DeleteItem { common.ItemId id = 1; }

message ItemDeletedEvent { common.ItemId itemId = 1; }
message ItemAddedEvent { Item item = 1; }
message ItemUpdatedEvent { Item item = 1; }

message ItemEvent {
  oneof event {
    ItemAddedEvent addedEvent = 1;
    ItemDeletedEvent deleteEvent = 2;
    ItemUpdatedEvent updateEvent = 3;
  }
}

message InventoryServiceCall {
  oneof method {
    AddItem addItem = 1;
    GetItem getItem = 2;
    GetInventory getInventory = 3;
    UpdateItem updateItem = 4;
    DeleteItem deleteItem = 5;
  }
  common.MetaData metaData = 6;
}

message InventoryServiceResponse {
  oneof data {
    common.ItemId itemId = 1;
    Item item = 2;
    common.ResponseStatus status = 3;
    Inventory inv = 4;
  }
  common.MetaData metaData = 5;
}