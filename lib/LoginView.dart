import 'package:flutter/material.dart';
import 'package:web_minigame/services/auth_events.dart';
import 'package:web_minigame/services/monster_service.dart';
import 'package:web_minigame/services/auth_service.dart';

import 'main.dart';

class MyLoginView extends StatefulWidget {
  final String nextPath;
  MyLoginView(this.nextPath);

  @override
  State<StatefulWidget> createState() {
    return LoginState(nextPath);
  }
}

class LoginState extends State<MyLoginView> {
  bool isLoggingIn = false;
  final String nextPath;

  AuthService authService;
  var usernameController = TextEditingController();
  var passwordController = TextEditingController();

  MonsterService monService;
  LoginState(this.nextPath);

  @override
  Widget build(BuildContext context) {
    authService = getIt.get<AuthService>();

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Form(
                child: Column(
              children: <Widget>[
                TextFormField(controller: usernameController),
                TextFormField(
                  style: TextStyle(color: Colors.black),
                  controller: passwordController,
                  obscureText: true,
                ),
                RaisedButton(
                  color: Colors.blue,
                  child: Text('Login'),
                  onPressed: () async => {
                    setState(() => {isLoggingIn = true}),
                    authService.add(LoginEvent()
                      ..username = usernameController.text
                      ..password = 'sdfjkldfsjkl'),
                    handleLogin()
                  },
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }

  Future<bool> handleLogin() async {
    var result = await authService.loggedIn.first;

    print(result);
    if (!result) {
      print('wrong password');
      return false;
    }
    await Navigator.pushNamed(context, nextPath);

    return true;
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    usernameController.dispose();
    super.dispose();
  }
}

//Navigator.pushNamed(context, '/dashboard')
