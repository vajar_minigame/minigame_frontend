import 'package:flutter/material.dart';
import 'package:proto/api/common.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:proto/api/monster.pb.dart';

typedef SelectedPokos = void Function(List<MonId> mons);

class MonsterList extends StatefulWidget {
  final Stream<Map<MonId, ValueStream<Monster>>> monStream;

  final SelectedPokos callback;
  MonsterList(this.monStream, this.callback);

  @override
  State<StatefulWidget> createState() {
    return _MonsterListState(monStream, callback);
  }
}

class _MonsterListState extends State<MonsterList> {
  final Stream<Map<MonId, ValueStream<Monster>>> monStream;

  SelectedPokos callback;

  List<MonId> selectedMons = [];

  _MonsterListState(this.monStream, this.callback);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        builder:
            (context, AsyncSnapshot<Map<MonId, ValueStream<Monster>>> snap) {
          if (snap.data == null || snap.hasError) {
            return Text('Loading');
          }

          var monList = snap.data;
          var mons = <Widget>[];

          for (var monId in monList.keys) {
            var mon = monList[monId];

            if (mon.value.hasActivity()) {
              continue;
            }
            mons.add(ChoiceChip(
              label: Text(mon.value.name),
              selected: selectedMons.contains(monId),
              onSelected: (selected) {
                print(selected);
                if (selected == false) {
                  setState(() {
                    selectedMons.remove(monId);
                    callback(selectedMons);
                  });
                } else {
                  setState(() {
                    selectedMons.add(monId);
                    callback(selectedMons);
                  });
                }
              },
            ));
          }
          return Container(
              width: 500, height: 500, child: ListView(children: mons));
        },
        stream: monStream);
  }
}
