import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:web_minigame/InventoryOverView.dart';
import 'package:web_minigame/MonsterOverView.dart';

import 'QuestOverView.dart';

class MyStartView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Row(
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: MonsterOverView(),
            ),
          ),
          Flexible(
            child: InventoryOverView(),
          ),
          Flexible(child: QuestOverView()),
        ],
      ),
    );
  }
}
