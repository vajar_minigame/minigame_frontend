import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:proto/api/common.pb.dart';

import 'package:web_minigame/data/monsterContainer_event.dart';

import 'monster_state.dart';

class MonsterBloc extends Bloc<MonsterContainerEvent, MonsterState> {
  StreamSubscription<MonsterContainerEvent> monSub;

  UserId userId;

  MonsterBloc(Stream monStream, this.userId) : super(MonsterLoading()) {
    monSub = monStream.listen((event) {});
  }

  @override
  Stream<MonsterState> mapEventToState(MonsterContainerEvent event) async* {
    if (event is MonsterUpdateEvent) {
      yield MonsterLoaded(event.m);
    }
  }
}
