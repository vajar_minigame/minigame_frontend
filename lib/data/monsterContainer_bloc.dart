import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/monster.pb.dart';
import 'package:web_minigame/auth/bloc.dart';
import 'package:web_minigame/data/monsterContainer_state.dart';
import 'package:web_minigame/data/monster_bloc.dart';
import 'package:web_minigame/services/monster_service.dart';
import 'package:web_minigame/utils/MyLogger.dart' as log;

import 'monsterContainer_event.dart';

class MonsterContainerBloc extends Bloc<MonsterContainerEvent, MonsterContainerState> {
  SimpleLogger logger = log.mylogger;
  final AuthBloc _auth;

  StreamSubscription<MonsterEvent> monStream;

  MonsterService monRepo;

  MonsterContainerBloc(this.monRepo, this._auth) : super(MonsterContainerLoading()) {
    _getStream();
    fetchRelevantMons();
  }

  void _getStream() async {
    monStream = monRepo.monEventStream.listen((event) {
      add(MonsterContainerEventHelper.eventConverter(event));
    });
  }

  void fetchRelevantMons() async {
    await for (var authInfo in _auth) {
      if (authInfo is Authenticated) {
        _loadMonByUserID(UserId()..id = authInfo.user.id);
      }
    }
  }

  @override
  Stream<MonsterContainerState> mapEventToState(MonsterContainerEvent event) async* {
    logger.info('new monsterdata is here ${event}');
    final currentState = state;
    if (state is MonsterContainerLoading) {
    } else if (currentState is MonsterContainerLoaded) {
      var currMons = currentState.map;
      if (event is MonsterAddeEvent) {
        var mon = event.m;

        currMons[MonId()..id = mon.id] = MonsterBloc(monRepo.monEventStream, UserId()..id = mon.playerID);
        yield MonsterContainerLoaded(currMons);
      } else if (event is MonsterUpdateEvent) {
        var mon = event.m;
        currMons[mon.id].add(event);
      } else if (event is MonsterDeleteEvent) {
        currMons.remove(event.m);
        yield MonsterContainerLoaded(currMons);
      }
    } else if (event is MonsterBulkUpdate) {
      for (var m in event.list) {
        if (currentState.map.containsKey(MonId()..id = m.id)) {
          add(MonsterUpdateEvent(m));
        } else {
          add(MonsterAddeEvent(m));
        }
      }
    }
  }

  Future<MonsterBloc> getMonsterById(MonId id) async {
    final currentState = state;

    if (!currentState.map.containsKey(id)) {
      var mon = await monRepo.GetMonByID(id);
      var event = MonsterAddeEvent(mon);
      add(event);
    }

    return state.map[id];
  }

  void _loadMonByUserID(UserId id) async {
    var mons = await monRepo.GetMonByUserID(id);

    add(MonsterBulkUpdate(mons));
  }

  Stream<MonsterContainerState> GetMonByUserIDStream(UserId id) {
    _loadMonByUserID(id);
    return map((event) {
      var userMons = <MonId, MonsterBloc>{};
      if (event is MonsterContainerLoaded) {
        for (var entry in event.map.entries) {
          if (entry.value.userId == id) {
            userMons[entry.key] = entry.value;
          }
        }
      }
      return MonsterContainerLoaded(userMons);
    });
  }
}
