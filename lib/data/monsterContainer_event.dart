import 'package:proto/api/common.pb.dart';
import 'package:proto/api/monster.pb.dart';

abstract class MonsterContainerEvent {}

class MonsterUpdateEvent extends MonsterContainerEvent {
  Monster m;
  MonsterUpdateEvent(this.m);
}

class MonsterDeleteEvent extends MonsterContainerEvent {
  MonId m;
  MonsterDeleteEvent(this.m);
}

class MonsterAddeEvent extends MonsterContainerEvent {
  Monster m;
  MonsterAddeEvent(this.m);
}

class MonsterBulkUpdate extends MonsterContainerEvent {
  List<Monster> list;
  MonsterBulkUpdate(this.list);
}

class UndefinedEvent extends MonsterContainerEvent {}

class MonsterContainerEventHelper {
  static MonsterContainerEvent eventConverter(MonsterEvent e) {
    if (e.hasAddedEvent()) {
      return MonsterAddeEvent(e.addedEvent.mon);
    } else if (e.hasDeleteEvent()) {
      return MonsterDeleteEvent(e.deleteEvent.monId);
    } else if (e.hasUpdateEvent()) {
      return MonsterUpdateEvent(e.updateEvent.mon);
    }
    return UndefinedEvent();
  }
}
