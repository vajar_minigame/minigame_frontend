import 'package:proto/api/monster.pb.dart';

abstract class MonsterState {}

class MonsterLoading extends MonsterState {}

class MonsterLoaded extends MonsterState {
  Monster mon;
  MonsterLoaded(this.mon);
}
