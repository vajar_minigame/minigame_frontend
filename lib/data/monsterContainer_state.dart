import 'package:equatable/equatable.dart';
import 'package:proto/api/common.pb.dart';
import 'package:web_minigame/data/monster_bloc.dart';

abstract class MonsterContainerState {
  Map<MonId, MonsterBloc> map = {};
  MonsterContainerState();
}

class MonsterContainerLoading extends MonsterContainerState {}

class MonsterContainerLoaded extends MonsterContainerState {
  MonsterContainerLoaded(Map<MonId, MonsterBloc> map) {
    this.map = map;
  }
}
