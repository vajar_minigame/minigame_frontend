import 'package:flutter/material.dart';
import 'package:proto/api/common.pb.dart';
import 'package:web_minigame/services/monster_service.dart';
import 'package:web_minigame/services/quest_service.dart';

import 'MonsterList.dart';

import 'main.dart';

class StartQuestView extends StatefulWidget {
  final QuestId questId;

  StartQuestView(this.questId);

  @override
  _StartQuestViewState createState() => _StartQuestViewState(questId);
}

class _StartQuestViewState extends State<StartQuestView> {
  final QuestId questId;
  var monService = getIt.get<MonsterService>();
  var questService = getIt.get<QuestService>();
  List<MonId> selectedMons = [];

  _StartQuestViewState(this.questId);

  void selectionChanged(List<MonId> mons) {
    setState(() {
      selectedMons = mons;
    });
  }

  void onStartQuest() {
    questService.startQuest(questId, selectedMons);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SimpleDialog(
        children: [
          MonsterList(monService.getLoggedInMonstersStream(), selectionChanged),
          RaisedButton(
            child: Text('Start'),
            onPressed: () {
              if (selectedMons.isEmpty) {
                return null;
              }
              return onStartQuest;
            }(),
          )
        ],
        title: Text('Select Monsters'),
      ),
    );
  }
}
