import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/quest.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:web_minigame/StartQuestView.dart';

import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/monster_service.dart';
import 'package:web_minigame/services/quest_service.dart';

class QuestOverView extends StatefulWidget {
  QuestOverView();

  @override
  State<StatefulWidget> createState() {
    return _QuestOverViewState();
  }
}

class _QuestOverViewState extends State<QuestOverView> {
  Map<QuestType, Map<QuestId, BehaviorSubject<Quest>>> currQuests;
  MonsterService monService;
  QuestService questService;
  ValueStream<Map<QuestId, BehaviorSubject<Quest>>> questListStream;

  _QuestOverViewState() {
    var getIt = GetIt.instance;
    var authService = getIt.get<AuthService>();
    monService = getIt.get<MonsterService>();
    questService = getIt.get<QuestService>();
    questService.GetQuestsByUserID(authService.getUserID());
    questListStream = questService.getUserQuestStream();
    questListStream.listen(newQuestList);
  }

  // through the stream a new inventorywas made
  void newQuestList(Map<QuestId, BehaviorSubject<Quest>> data) {
    currQuests = {};
    currQuests[QuestType.activeQuest] = {};
    currQuests[QuestType.potentialQuest] = {};

    for (var questId in data.keys) {
      var quest = data[questId].value;

      if (quest.hasActiveQuest()) {
        setState(() {
          currQuests[QuestType.activeQuest][quest.id] =
              BehaviorSubject.seeded(quest);
          print(quest.activeQuest);
        });
      } else {
        setState(() {
          currQuests[QuestType.potentialQuest][quest.id] =
              BehaviorSubject.seeded(quest);
        });
      }
    }
  }

  void onEvent(QuestEvent e) {
    if (e.hasCompletedEvent()) {
    } else if (e.hasDeletedEvent()) {
      setState(() {
        currQuests.remove(e.deletedEvent.questId);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> activeQuests = [];

    List<Widget> potentialQuests = [];

    if (currQuests == null) {
      return Text('Loading');
    }

    var activeQuestList = currQuests[QuestType.activeQuest];
    for (var questID in activeQuestList.keys) {
      var quest = activeQuestList[questID];
      activeQuests.add(ExpansionTile(
        title: Text(quest.value.recommendedStrength.toString()),
        children: <Widget>[
          Text(quest.value.difficulty.toString()),
          Text(quest.value.playerID.toString())
        ],
      ));
    }

    var potentialQuestList = currQuests[QuestType.potentialQuest];

    for (var questID in potentialQuestList.keys) {
      var quest = potentialQuestList[questID];
      potentialQuests.add(ExpansionTile(
        title: Text(quest.value.recommendedStrength.toString()),
        children: <Widget>[
          Text(quest.value.difficulty.toString()),
          Text(quest.value.playerID.toString()),
          RaisedButton(
            child: Text('Start mission!'),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return StartQuestView(questID);
                  });
            },
          )
        ],
      ));
    }

    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Text('Potential Quests'),
              Expanded(
                child: ListView(
                  children: potentialQuests,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              Text('Active Quests'),
              Expanded(
                child: ListView(
                  children: activeQuests,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
