import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/battle.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:web_minigame/MonsterDetailView.dart';
import 'package:proto/api/common.pb.dart';
import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/battle_service.dart';
import 'package:web_minigame/services/monster_service.dart';

class ActiveBattleView extends StatefulWidget {
  final ValueStream<Battle> battle;
  ActiveBattleView(this.battle, {Key key}) : super(key: key);

  @override
  _ActiveBattleViewState createState() => _ActiveBattleViewState(battle);
}

class _ActiveBattleViewState extends State<ActiveBattleView> {
  ValueStream<Battle> battle;
  var getIt = GetIt.instance;
  MonsterService monService;
  AuthService authService;
  BattleService battleService;

  MonId source;
  MonId target;

  _ActiveBattleViewState(this.battle) {
    monService = getIt.get<MonsterService>();
    authService = getIt.get<AuthService>();
    battleService = getIt.get<BattleService>();
  }

  void test(var monId) {
    setState(() {
      target = monId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: battle,
      builder: (ctx, AsyncSnapshot<Battle> snap) {
        if (snap.hasError || snap.data == null) {
          return Text('loading');
        }
        if (snap.data.state.activeBattle.turnQueue.isEmpty) {
          return Text('battle is over=?=?');
        }
        var nextMon = monService
            .getMonStreamByID(snap.data.state.activeBattle.turnQueue[0]);

        if (nextMon.value == null) {
          nextMon.listen((data) => setState(() => {}));
          return Text('loading');
        } else if (nextMon.value.playerID == authService.getUserID().id) {
          source = MonId()..id = nextMon.value.id;
        } else {
          source = null;
        }

        var monster = <MonId, MonsterDetailView>{};
        for (var id in snap.data.teamA.mons) {
          var mon = monService.getMonStreamByID(id);
          if (id.id == nextMon.value.id || id == source || id == target) {
            monster[id] = MonsterDetailView(mon, true);
          } else {
            monster[id] = MonsterDetailView(mon, false);
          }
        }
        for (var id in snap.data.teamB.mons) {
          var mon = monService.getMonStreamByID(id);
          if (id.id == nextMon.value.id || id == source || id == target) {
            monster[id] = MonsterDetailView(mon, true);
          } else {
            monster[id] = MonsterDetailView(mon, false);
          }
        }

        return Column(
          children: <Widget>[
            Text('Battle: ${snap.data.id}'),
            // split teams left team a; right team b
            Row(
              children: <Widget>[
                Flexible(
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.45,
                    color: Colors.greenAccent,
                    child: Column(
                      children: <Widget>[
                        Text('Team of player ${snap.data.teamA.users}'),
                        for (var monId in snap.data.teamA.mons) ...{
                          GestureDetector(
                            child: monster[monId],
                            onTap: () {
                              print('was tapped');
                              test(monId);
                            },
                          )
                        }
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    children: <Widget>[
                      Text('Team of player ${snap.data.teamB.users}'),
                      for (var monId in snap.data.teamB.mons) ...{
                        GestureDetector(
                            child: monster[monId],
                            onTap: () {
                              print(monster[monId].selected.toString() +
                                  '       the value');
                              print('was tapped');
                              test(monId);
                            })
                      }
                    ],
                  ),
                ),
              ],
            ),

            if (nextMon?.value?.playerID == authService.getUserID().id)
              RaisedButton(
                child: Text('Execute'),
                onPressed: () {
                  print('attaaaaack');
                  if (source != null && target != null) {
                    battleService.attackMon(
                        source, target, BattleId()..id = snap.data.id);
                  }

                  source = null;
                  target = null;
                },
              )
          ],
        );
      },
    );
  }
}
