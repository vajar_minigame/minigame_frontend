import 'package:flutter/material.dart';
import 'package:proto/api/common.pb.dart';
import 'package:web_minigame/services/battle_service.dart';

import 'package:web_minigame/services/monster_service.dart';

import '../MonsterList.dart';
import '../main.dart';

class StartBattleView extends StatefulWidget {
  final BattleId battleId;

  StartBattleView(this.battleId);

  @override
  _StartBattleViewState createState() => _StartBattleViewState(battleId);
}

class _StartBattleViewState extends State<StartBattleView> {
  final BattleId battleId;
  var monService = getIt.get<MonsterService>();
  var battleService = getIt.get<BattleService>();
  List<MonId> selectedMons = [];

  _StartBattleViewState(this.battleId);

  void selectionChanged(List<MonId> mons) {
    setState(() {
      selectedMons = mons;
    });
  }

  void onStartBattle() {
    print(battleId);
    battleService.startBattle(battleId, selectedMons);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SimpleDialog(
        children: [
          MonsterList(monService.getLoggedInMonstersStream(), selectionChanged),
          RaisedButton(
            child: Text('Start'),
            onPressed: () {
              if (selectedMons.isEmpty) {
                return null;
              }
              return onStartBattle;
            }(),
          )
        ],
        title: Text('Select Monsters'),
      ),
    );
  }
}
