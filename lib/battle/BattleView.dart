import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/battle.pb.dart';
import 'package:rxdart/rxdart.dart';

import 'package:proto/api/common.pb.dart';
import 'package:web_minigame/battle/ActiveBattleView.dart';
import 'package:web_minigame/services/battle_service.dart';

import 'StartBattleView.dart';

class BattleView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BattleViewState();
  }
}

class _BattleViewState extends State<BattleView> {
  BattleService battleService = GetIt.instance.get<BattleService>();
  BehaviorSubject<List<BehaviorSubject<Battle>>> battleStream;

  _BattleViewState() {
    battleStream = battleService.battleStream;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        builder: (context, AsyncSnapshot<List<BehaviorSubject<Battle>>> snap) {
          if (snap.hasError || snap.data == null || snap.data.isEmpty) {
            return Text('loading');
          }
          var activeBattles =
              snap.data.where((b) => b.value.state.hasActiveBattle()).toList();
          var pendingBattles = snap.data
              .where((b) => b.value.state.hasActiveBattle() == false)
              .toList();

          return Column(children: [
            Flexible(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 100,
                color: Colors.blueAccent,
                child: ListView.builder(
                    itemCount: pendingBattles.length,
                    itemBuilder: (context, index) {
                      var id = BattleId()..id = pendingBattles[index].value.id;
                      return Row(children: [
                        Card(
                            child: Column(children: <Widget>[
                          Text('${snap.data[index].value.id}'),
                          RaisedButton(
                            child: Text('Start'),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return StartBattleView(id);
                                  });
                            },
                          ),
                        ]))
                      ]);
                    }),
              ),
            ),
            if (activeBattles.isNotEmpty) ...{
              Container(
                width: MediaQuery.of(context).size.width * 1,
                height: 800,
                child: ActiveBattleView(battleService.getBattleStream(
                    BattleId()..id = activeBattles[0].value.id)),
              )
            }
          ]);
        },
        stream: battleStream,
      ),
    );
  }
}
