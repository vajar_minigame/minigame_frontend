import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/common.pb.dart';
import 'package:rxdart/rxdart.dart';

import 'package:web_minigame/MonsterDetailView.dart';
import 'package:proto/api/monster.pb.dart';

import 'package:web_minigame/services/interaction_service.dart';
import 'package:web_minigame/services/monster_service.dart';

class MonsterOverView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MonsterOverViewState();
  }
}

class _MonsterOverViewState extends State<MonsterOverView> {
  InteractionService interactionService;
  MonsterService monService;

  Stream<Map<MonId, ValueStream<Monster>>> monStream;

  _MonsterOverViewState() {
    var getIT = GetIt.instance;
    interactionService = getIT.get<InteractionService>();
    monService = getIT.get<MonsterService>();

    monStream = monService.getLoggedInMonstersStream();
  }

  @override
  Widget build(BuildContext context) {
    var mons = <Widget>[];

    return StreamBuilder(
        stream: monStream,
        builder: (context, AsyncSnapshot<Map<MonId, Stream<Monster>>> snap) {
          var monList = snap.data;

          if (snap.hasError || monList == null || monList.isEmpty) {
            return Text('Loading');
          }

          for (var monId in monList.keys) {
            var mon = monList[monId];
            mons.add(Card(
              child: MonsterDetailView(mon, false),
            ));
          }
          return ListView(
            children: mons,
          );
        });
  }
}
