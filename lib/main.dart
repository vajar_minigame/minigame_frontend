import 'package:flutter/material.dart';
import 'package:grpc/grpc_web.dart';
import 'package:web_minigame/services/battle_service.dart';
import 'package:web_minigame/services/interaction_service.dart';
import 'package:web_minigame/services/item_service.dart';
import 'package:web_minigame/services/monster_service.dart';
import 'package:web_minigame/services/quest_service.dart';
import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/user_service.dart';
import 'package:web_minigame/services/websocket_service.dart';
import 'package:web_minigame/startView.dart';
import 'package:get_it/get_it.dart';
import 'LoginView.dart';
import 'battle/BattleView.dart';

GetIt getIt = GetIt.instance;

void main() {
  var grpcChannel =
      GrpcWebClientChannel.xhr(Uri.parse('http://localhost:30808'));
  final authService = AuthService(grpcChannel);

  final grpcService = GrpcService(grpcChannel, authService);

  getIt.registerLazySingleton(() {
    return QuestService(authService, grpcService);
  });
  getIt.registerLazySingleton(() {
    return UserService(grpcChannel, authService);
  });
  getIt.registerLazySingleton(() {
    return MonsterService(authService, grpcService);
  });
  getIt.registerLazySingleton(() {
    return InventoryService(authService, grpcService);
  });
  getIt.registerLazySingleton(() {
    return InteractionService(authService, grpcService.getGrpcChannel());
  });

  getIt.registerLazySingleton(() {
    return BattleService(grpcService, authService);
  });

  getIt.registerSingleton(grpcService);

  getIt.registerSingleton(authService);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var authservice = getIt.get<AuthService>();
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(primarySwatch: Colors.blue, buttonColor: Colors.green),
        routes: {
          '/': (context) => MyLoginView('/dashboard'),
          '/dashboard': (context) {
            if (authservice.getUser() == null) {
              return MyLoginView('/dashboard');
            } else {
              return MyStartView();
            }
          },
          '/battle': (context) {
            if (authservice.getUser() == null) {
              return MyLoginView('/battle');
            } else {
              return BattleView();
            }
          }
        });
  }
}
