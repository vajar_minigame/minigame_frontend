import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/inventory.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:proto/api/monster.pb.dart';
import 'package:web_minigame/services/interaction_service.dart';

class MonsterDetailView extends StatelessWidget {
  final ValueStream<Monster> monStream;
  final bool selected;
  MonsterDetailView(this.monStream, this.selected);

  Color chooseColor() {
    if (this.monStream.value.status.isAlive == false) {
      print("the mon is ded");
      return Colors.grey.shade600;
    }
    if (selected) {
      return Colors.blueAccent;
    }

    return null;
  }

  monView(Monster mon) {
    var dragTarget = Card(
        color: chooseColor(),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                mon.name,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            Table(children: <TableRow>[
              TableRow(
                children: [
                  Text('PlayerID'),
                  Text(mon.playerID.toString()),
                ],
              ),
              TableRow(children: [
                Text('HP'),
                LinearProgressIndicator(
                    value:
                        mon.battleValues.remainingHp / mon.battleValues.maxHp),
                //Text(
                //   "${mon.battleValues.remainingHp.toString()}/${mon.battleValues.maxHp.toString()}")
              ]),
              TableRow(children: [
                Text('Last update'),
                Text(mon.lastUpdate.toString())
              ]),
              TableRow(
                children: [
                  Text('Saturation'),
                  LinearProgressIndicator(
                    value: mon.bodyValues.remainingSaturation /
                        mon.bodyValues.maxSaturation,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                    backgroundColor: Colors.lightGreen,
                  )
                ],
              ),
              TableRow(children: [
                Text('Activity'),
                if (mon.activity.hasBattleID()) ...{
                  Text(mon.activity.battleID.toString()),
                } else if (mon.activity.hasQuestId()) ...{
                  Text(mon.activity.questId.toString()),
                } else ...{
                  Text('Idle')
                }
              ]),
            ])
          ],
        ));

    return dragTarget;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snap) {
        Monster mon = snap.data;
        if (snap.hasError || mon == null) {
          return Text('Error!');
        } else {
          return DragTarget<Item>(
            builder: (BuildContext context, List incoming, List rejected) {
              return monView(mon);
            },
            onWillAccept: (data) {
              return true;
            },
            onAccept: (data) {
              print(data.id);
              var getIt = GetIt.instance;
              getIt
                  .get<InteractionService>()
                  .consumeItem(MonId()..id = mon.id, ItemId()..id = data.id);
            },
            onLeave: (data) {},
          );
        }
      },
      stream: monStream,
    );
  }
}
