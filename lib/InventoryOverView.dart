import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/inventory.pb.dart';
import 'package:rxdart/subjects.dart';

import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/item_service.dart';

class InventoryOverView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InventoryOverViewState();
  }
}

class _InventoryOverViewState extends State<InventoryOverView> {
  Map<ItemId, BehaviorSubject<Item>> currItems;

  Stream<Inventory> invStream;
  InventoryService invService;
  AuthService auth;

  _InventoryOverViewState() {
    var getIt = GetIt.instance;
    invService = getIt<InventoryService>();
    auth = getIt<AuthService>();
    var userID = UserId()..id = auth.getUser().id;
    // just trigger it
    // the result will arrive through events
    invService.getInventory(userID);

    invStream = invService.inventoryStream;
    invStream.listen(newInv);
  }

  // through the stream a new inventorywas made
  void newInv(Inventory data) {
    print('new inventory data is there');
    currItems = {};
    for (var item in data.items) {
      currItems[ItemId()..id = item.id] = BehaviorSubject.seeded(item);
    }

    setState(() {
      currItems = currItems;
    });
  }

  @override
  Widget build(BuildContext context) {
    var items = <Widget>[];

    if (currItems == null) {
      return Text('Loading');
    }

    for (var itemId in currItems.keys) {
      var item = currItems[itemId];
      items.add(Card(
          child: Draggable<Item>(
        feedback: Material(child: Text(item.value.itemDef.type)),
        child: (Text(
          item.value.itemDef.type,
        )),
        data: item.value,
      )));
    }

    return ListView(
      children: items,
    );
  }
}
