import 'dart:async';

import 'package:proto/api/common.pb.dart';
import 'package:proto/api/monster.pbgrpc.dart';
import 'package:rxdart/rxdart.dart';

import 'package:rxdart/subjects.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:proto/api/monster.pb.dart';

import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/websocket_service.dart';
import 'package:web_minigame/utils/MyLogger.dart' as log;

class MonsterService {
  Stream<MonsterEvent> _monEventStream;
  GrpcService channel;
  MonsterServicesClient monsterClient;
  AuthService auth;
  SimpleLogger logger = log.mylogger;

  BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>> currAllMons;

  MonsterService(this.auth, this.channel) {
    monsterClient = MonsterServicesClient(channel.getGrpcChannel(),
        options: auth.getAuthCallOption());

    GetMonByUserID(auth.getUserID());
    _monEventStream =
        channel.getEventStream((e) => e.hasMonEvent(), (e) => e.monEvent);
    _monEventStream.listen((e) => _newMonsterEvent(e));

    currAllMons =
        BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>>.seeded({});
  }
  Stream<MonsterEvent> get monEventStream => _monEventStream;
  void addMon(Monster mon) {
    throw UnimplementedError();
  }

  Future<Monster> GetMonByID(MonId id) async {
    var response = await monsterClient.getMonByID(id);
    currAllMons.value[id].add(response);
    return response;
  }

  Future<List<Monster>> GetMonByUserID(UserId id) async {
    var response = await monsterClient.getMonsByUserID(id);

    if (auth.getUserID() == id) {
      _updateMonsters(response.monsters);
    }

    return response.monsters;
  }

  void _newMonsterEvent(MonsterEvent event) {
    logger.info('new monsterdata is here ${event}');
    if (event.hasAddedEvent()) {
      var addEvent = event.addedEvent;
      var currMons = currAllMons.value;
      var monS = BehaviorSubject.seeded(addEvent.mon);
      currMons[MonId()..id = addEvent.mon.id] = monS;
      currAllMons.add(currMons);
    } else if (event.hasUpdateEvent()) {
      var updateEvent = event.updateEvent;
      var subject = currAllMons.value[MonId()..id = updateEvent.mon.id];
      subject.add(updateEvent.mon);
    } else if (event.hasDeleteEvent()) {
      var deletedEvent = event.deleteEvent;
      var currMons = currAllMons.value;
      currMons.remove(deletedEvent.monId);
      currAllMons.add(currMons);
    }
  }

  Stream<Map<MonId, ValueStream<Monster>>> getMonStreamByUserId(UserId id) {
    var test = currAllMons.map((Map<MonId, BehaviorSubject<Monster>> event) {
      var userMons = <MonId, ValueStream<Monster>>{};
      var currMons = event;

      for (var mon in currMons.entries) {
        if (mon.value.hasValue && mon.value.value.playerID == id.id) {
          userMons[MonId()..id = mon.value.value.id] = mon.value;
        }
      }
      return userMons;
    });

    return test;
  }

  ValueStream<Monster> getMonStreamByID(MonId id) {
    var mon = currAllMons.value.putIfAbsent(id, () {
      GetMonByID(id);
      return BehaviorSubject<Monster>.seeded(null);
    });

    return mon;
  }

  void _updateMonsters(List<Monster> data) {
    var allMons = currAllMons.value;
    for (var mon in data) {
      var monS = BehaviorSubject.seeded(mon);
      allMons[MonId()..id = mon.id] = monS;
    }

    currAllMons.add(allMons);
  }

  Stream<Map<MonId, ValueStream<Monster>>> getLoggedInMonstersStream() {
    return getMonStreamByUserId(auth.getUserID());
  }

  void updateMon(Monster mon) {}

  void deleteMon(MonId id) {}
}
