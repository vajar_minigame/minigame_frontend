import 'package:grpc/grpc_web.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/user.pbgrpc.dart';
import 'package:web_minigame/services/auth_service.dart';

class UserService {
  GrpcWebClientChannel channel;
  UserControllerClient userClient;
  UserService(this.channel, AuthService authservice) {
    var blob = authservice.getAuthCallOption();
    userClient = UserControllerClient(channel, options: blob);
  }

  Future<User> getUser(UserId id) async {
    try {
      var res = await userClient.getUser(id);
      return res;
    } catch (e) {
      print('error occured getting user $e');
      return null;
    }
  }
}
