class AuthEvent {}

class LoginEvent extends AuthEvent {
  String username;
  String password;
}

class RegisterEvent extends AuthEvent {
  String username;
  String password;
}

class LogoutEvent extends AuthEvent {}
