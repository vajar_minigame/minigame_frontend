import 'package:grpc/grpc_web.dart';
import 'package:proto/api/controller.pb.dart';
import 'package:proto/api/eventStream.pbgrpc.dart';
import 'package:rxdart/subjects.dart';

import 'auth_service.dart';

class GrpcService {
  GrpcWebClientChannel grpcChannel;
  EventStreamingServiceClient client;

  ResponseStream<Event> stream;
  BehaviorSubject<Event> outStream;
  AuthService auth;

  GrpcService(this.grpcChannel, this.auth) {
    auth.loggedIn.listen((loggedIn) {
      init(loggedIn);
    });

    outStream = BehaviorSubject();
  }
  GrpcWebClientChannel getGrpcChannel() {
    return grpcChannel;
  }

  void init(bool loggedIn) {
    if (!loggedIn) {
      return;
    }
    client = EventStreamingServiceClient(grpcChannel,
        options: auth.getAuthCallOption());
    stream = client.eventStream(auth.getUserID());
    stream.listen((value) => _newEvent(value));
  }

  void _newEvent(Event e) {
    outStream.add(e);
  }

  Stream<T> getEventStream<T>(
      bool Function(Event) filter, T Function(Event) mapping) {
    var newStream = outStream.where(filter).map(mapping);
    return newStream;
  }
}
