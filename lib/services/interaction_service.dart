import 'dart:async';

import 'package:grpc/grpc_web.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/interactions.pbgrpc.dart';

import 'package:web_minigame/services/auth_service.dart';

class InteractionService {
  InteractionServiceClient interactionClient;
  final GrpcWebClientChannel _channel;

  InteractionService(AuthService auth, this._channel) {
    interactionClient =
        InteractionServiceClient(_channel, options: auth.getAuthCallOption());
  }

  Future<ResponseStatus> consumeItem(MonId monID, ItemId itemID) {
    var func = MonConsumeItemRequest();
    func.monId = monID;
    func.itemId = itemID;

    var response = interactionClient.monsterConsumeItem(func);

    return response;
  }
}
