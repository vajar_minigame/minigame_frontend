import 'dart:async';

import 'package:proto/api/common.pb.dart';
import 'package:proto/api/controller.pb.dart';
import 'package:proto/api/quest.pb.dart';
import 'package:proto/api/quest.pbgrpc.dart';
import 'package:rxdart/rxdart.dart';

import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/websocket_service.dart';

enum QuestType { activeQuest, potentialQuest }

class QuestService {
  BehaviorSubject<Map<QuestId, BehaviorSubject<Quest>>> currUserQuests;
  GrpcService channel;

  Stream<QuestEvent> _eventStream;
  Stream<QuestEvent> get eventStream => _eventStream;

  QuestServicesClient questclient;
  PublishSubject<Quest> questUpdatesStream;
  AuthService auth;

  QuestService(this.auth, this.channel) {
    questclient = QuestServicesClient(channel.getGrpcChannel(),
        options: auth.getAuthCallOption());

    questUpdatesStream = PublishSubject<Quest>();
    currUserQuests = BehaviorSubject<Map<QuestId, BehaviorSubject<Quest>>>();

    GetQuestsByUserID(auth.getUserID());

    _eventStream = channel.getEventStream(
        (Event e) => e.hasQuestEvent(), (Event e) => e.questEvent);

    _eventStream.listen(onQuestEvent);
  }

  void newQuestList(List<Quest> data) {
    var currQuests = <QuestId, BehaviorSubject<Quest>>{};
    for (var quest in data) {
      currQuests[quest.id] = BehaviorSubject.seeded(quest);
    }

    currUserQuests.add(currQuests);
  }

  Stream<Map<QuestId, Stream<Quest>>> GetLoggedInUserQuests() {
    if (currUserQuests == null) {
      var userID = UserId()..id = auth.getUser().id;
      GetQuestsByUserID(userID).then(newQuestList);
      currUserQuests = BehaviorSubject();
    }
    return currUserQuests;
  }

  void addQuest(Quest quest) {
    throw UnimplementedError();
  }

  Future<Quest> GetQuestByID(QuestId id) async {
    var response = await questclient.getQuestByID(id);

    return response;
  }

  Future<List<Quest>> GetQuestsByUserID(UserId id) async {
    var response = await questclient.getQuestByUserID(id);

    if (auth.getUserID() == id) {
      newQuestList(response.quests);
    }

    return response.quests;
  }

  Future<ResponseStatus> startQuest(QuestId questID, List<MonId> monIds) async {
    var func = StartQuestRequest();
    func.questId = questID;
    func.monsterIds.addAll(monIds);

    var response = await questclient.startQuest(func);

    return response;
  }

  void deleteQuest(ItemId id) {
    throw UnimplementedError();
  }

  void onQuestEvent(QuestEvent event) {
    if (currUserQuests == null) {
      return;
    }
    if (event.hasStartedEvent()) {
      var startEvent = event.startedEvent;

      var currQuests = currUserQuests.value;
      currQuests[startEvent.quest.id].add(startEvent.quest);

      //maybe necessary to indictate that it should switch lists
      currUserQuests.add(currQuests);
    } else if (event.hasCompletedEvent()) {
      var completeEvent = event.completedEvent;
      var currQuests = currUserQuests.value;
      currQuests.remove(completeEvent.questId);
      currUserQuests.add(currQuests);
    } else if (event.hasDeletedEvent()) {
      var deltedEvent = event.deletedEvent;
      var currQuests = currUserQuests.value;
      currQuests.remove(deltedEvent.questId);
      currUserQuests.add(currQuests);
    } else if (event.hasUpdatedEvent()) {
      var updatedEvent = event.updatedEvent;

      var currQuests = currUserQuests.value;
      currQuests[updatedEvent.quest.id].add(updatedEvent.quest);

      //maybe necessary to indictate that it should switch lists
      currUserQuests.add(currQuests);
    } else {
      print('not implemented yet');
    }
  }

  void dispose() {
    // TODO: implement dispose
  }

  BehaviorSubject<Map<QuestId, BehaviorSubject<Quest>>> getUserQuestStream() {
    return currUserQuests;
  }
}
