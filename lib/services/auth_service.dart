import 'package:flutter/widgets.dart';
import 'package:grpc/grpc_web.dart';
import 'package:proto/api/auth.pbgrpc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/user.pb.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'auth_events.dart';
import 'auth_state.dart';

class AuthService extends Bloc<AuthEvent, AuthState> {
  User user;
  String token;

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final PublishSubject<bool> _loggedIn = PublishSubject<bool>();

  Stream<bool> get loggedIn => _loggedIn;

  GrpcWebClientChannel channel;

  AuthClient authClient;

  AuthService(this.channel) : super(LoggedOut()) {
    authClient = AuthClient(channel);
    // _handleSignIn();
  }

  CallOptions getAuthCallOption() {
    if (token != null) {
      return CallOptions(metadata: {'authorization': 'Bearer ' + token});
    }
    return null;
  }
/*
  Future<FirebaseUser> _handleSignIn() async {
    final googleUser = await _googleSignIn.signIn();
    print("login with gogol");
    final googleAuth = await googleUser.authentication;

    final credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    print("signed in " + user.displayName);
    return user;
  }
  */

  Future<void> login({@required String username, @required String password}) async {
    try {
      var res = await authClient.login(LoginCall()
        ..username = username
        ..password = password);

      token = res.token;
      user = res.user;
    } catch (e) {
      print(e);
      print('error occured logging in');
      _loggedIn.sink.add(false);
    }

    print('noooow');
    _loggedIn.sink.add(true);
  }

  Future<void> logout() async {
    user = null;
  }

  User getUser() {
    return user;
  }

  UserId getUserID() {
    var userID = UserId()..id = user.id;
    return userID;
  }

  void dispose() {}

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoginEvent) {
      await _googleSignIn.signIn();
      /*
      var user = await _auth(email: event.username, password: event.password);
      var token = await user.user.getIdToken();
      print(token.token);
      */
    }
    if (event is RegisterEvent) {
      var user = await _auth.createUserWithEmailAndPassword(email: event.username, password: event.password);

      yield LoggedIn()..user = user.user;
    }
  }
}
