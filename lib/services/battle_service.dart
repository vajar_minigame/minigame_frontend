import 'dart:collection';

import 'package:proto/api/battle.pb.dart';
import 'package:proto/api/battle.pbgrpc.dart';
import 'package:proto/api/controller.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:proto/api/common.pb.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:web_minigame/services/websocket_service.dart';
import 'package:web_minigame/utils/MyLogger.dart' as log;

import 'auth_service.dart';

class BattleService {
  SimpleLogger logger = log.mylogger;
  Stream<BattleEvent> battleEventStream;
  BehaviorSubject<List<BehaviorSubject<Battle>>> battleStream;

  final GrpcService channel;
  final AuthService auth;
  BattleServiceClient client;

// events can arrive out of order? in theory some clock thingy needed
//todo useful event system
  Queue<BattleEvent> eventBacklog = Queue();

  BattleService(this.channel, this.auth) {
    client = BattleServiceClient(channel.getGrpcChannel(),
        options: auth.getAuthCallOption());

    battleEventStream = channel.getEventStream<BattleEvent>(
        (Event e) => e.hasBattleEvent(), (Event e) => e.battleEvent);
    battleStream = BehaviorSubject<List<BehaviorSubject<Battle>>>();
    getUserBattles();
    battleEventStream.listen(onEvent,
        onError: (e, trace) => print("battlestream can't be opened! ${e}"));
  }

  Future<List<ValueStream<Battle>>> getUserBattles() async {
    BattleList battles;
    try {
      battles = await client.getBattleByUserID(auth.getUserID());
    } catch (e) {
      print(e);
    }

    var list = newBattleList(battles.battles);
    battleStream.add(list);
    return list;
  }

  Future<Battle> getBattle(BattleId id) async {
    var battle = await client.getBattleByID(id);

    return battle;
  }

  ValueStream<Battle> getBattleStream(BattleId id) {
    var index = battleStream.value.indexWhere((b) {
      return b.value.id == id.id;
    });
    return battleStream.value[index];
  }

  void onEvent(BattleEvent event) {
    //todo implement
    print('new battle event');

    if (event.hasTurnUpdate()) {
      // clear old turnqueue and write new one
      print('has turnupdate');
      var battleList = battleStream.value;
      var index = battleList.indexWhere((b) => b.value.id == event.id.id);
      var b = battleList[index].value;

      var newBattle = event.turnUpdate.b;

      if (newBattle.lastUpdate
          .toDateTime()
          .isBefore(b.lastUpdate.toDateTime())) {
        logger.warning("event arrives after later event");
        return;
      }
      battleList[index].add(event.turnUpdate.b);
    } else if (event.hasBattleStarted()) {
      print('new battle has started');
      // find battle and push out
      var battleList = battleStream.value;
      var index = battleList.indexWhere((b) => b.value.id == event.id.id);
      print('index of battle to modify ${index}');
      battleList[index].add(event.battleStarted.b);

      battleStream.add(battleList);
    } else if (event.hasBattleEnded()) {}
  }

  void startBattle(BattleId id, List<MonId> selectedMons) async {
    var request = BattleRequest()
      ..id = id
      ..monIds.addAll(selectedMons)
      ..userId = auth.getUserID();
    client.startBattle(request);
  }

  List<BehaviorSubject<Battle>> newBattleList(List<Battle> battles) {
    var list = <BehaviorSubject<Battle>>[];
    for (var b in battles) {
      var subject = BehaviorSubject<Battle>.seeded(b);
      list.add(subject);
    }
    return list;
  }

  Future<bool> attackMon(MonId source, MonId target, BattleId id) async {
    var att = Attack()
      ..source = source
      ..target = target;
    var res = await client.commandRequest(Command()
      ..attack = att
      ..battleId = id);
    return res.success;
  }
}
