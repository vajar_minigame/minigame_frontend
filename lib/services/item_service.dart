import 'dart:async';

import 'package:grpc/grpc_web.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/controller.pb.dart';
import 'package:proto/api/inventory.pbgrpc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:simple_logger/simple_logger.dart';
import 'package:web_minigame/services/auth_service.dart';
import 'package:web_minigame/services/websocket_service.dart';
import 'package:web_minigame/utils/MyLogger.dart' as log;

class InventoryService {
  SimpleLogger logger = log.mylogger;

  Stream<ItemEvent> _eventStream;

  BehaviorSubject<Inventory> inventoryStream;

  ResponseStream<ItemEvent> get eventStream => _eventStream;

  AuthService auth;
  GrpcService channel;
  ItemServiceClient itemClient;

  InventoryService(this.auth, this.channel) {
    logger.setLevel(
      Level.INFO,
      // Includes  caller info, but this is expensive.
      includeCallerInfo: true,
    );
    itemClient = ItemServiceClient(channel.getGrpcChannel(),
        options: auth.getAuthCallOption());

    var eventStream = channel.getEventStream<ItemEvent>(
        (Event e) => e.hasItemEvent(), (Event e) => e.itemEvent);
    inventoryStream = BehaviorSubject<Inventory>();
    _eventStream = eventStream;
    eventStream.listen(onEvent);
  }

  void onEvent(ItemEvent e) {
    var currInventory = inventoryStream.value;
    logger.info('new ItemEvent ${e}');
    if (e.hasAddedEvent()) {
      currInventory.items.add(e.addedEvent.item);
    } else if (e.hasDeleteEvent()) {
      currInventory.items
          .removeWhere((Item i) => i.id == e.deleteEvent.itemId.id);
    } else if (e.hasUpdateEvent()) {
      throw UnimplementedError();
    } else {
      throw UnimplementedError('The event is not implemented:${e}');
    }
    inventoryStream.add(currInventory);
  }

  void addItem(Item item) {}

  Future<Item> getItemById(ItemId id) async {
    var response = await itemClient.getItem(id);
    return response;
  }

  Future<Inventory> getInventory(UserId id) async {
    var response = await itemClient.getInventory(id);
    inventoryStream.add(response);

    return response;
  }

  void updateItem(Item item) {}

  void deleteItem(ItemId id) {}
}
