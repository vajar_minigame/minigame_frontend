import 'package:firebase_auth/firebase_auth.dart';

class AuthState {}

class LoggedIn extends AuthState {
  FirebaseUser user;
}

class LoggedOut extends AuthState {}
