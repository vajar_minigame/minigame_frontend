import 'dart:async';

import 'package:grpc/grpc_web.dart';
import 'package:proto/api/chat.pb.dart';
import 'package:proto/api/chat.pbgrpc.dart';
import 'package:proto/api/google/protobuf/timestamp.pb.dart';
import 'package:rxdart/rxdart.dart';
import 'package:proto/api/common.pb.dart';

import 'package:web_minigame/services/auth_service.dart';

class ChatService {
  final GrpcWebClientChannel channel;

  BehaviorSubject<Map<RoomId, List<Message>>> messageStream;
  var eventStreamSub;
  ChatServiceClient stub;
  final AuthService auth;
  ChatService(this.channel, this.auth) {
    try {
      stub = ChatServiceClient(channel);
      eventStreamSub = stub
          .getEventStream(UserId()..id = auth.getUser().id)
          .listen(OnChatEvent);
    } catch (e) {
      print(e);
    }

    var rooms = <RoomId, List<Message>>{};
    var room = RoomId()..id = 1;
    rooms[room] = test_create_list();

    messageStream = BehaviorSubject.seeded(rooms);
    print('end reached');
  }

  List<Message> test_create_list() {
    var room = RoomId()..id = 1;
    var msg = Message()
      ..body = 'bluba'
      ..roomId = room;

    var messages = <Message>[];
    for (var i = 0; i < 50; i++) {
      messages.add(msg);
    }
    return messages;
  }

  ValueStream<Map<RoomId, List<Message>>> getMessageStream() {
    return messageStream;
  }

  Future<bool> sendMessage(RoomId roomID, String message) async {
    try {
      var userID = UserId()..id = auth.user.id;
      await stub.sendMessage(Message()
        ..sender = userID
        ..roomId = roomID
        ..sentTime = Timestamp.fromDateTime(DateTime.now())
        ..body = message);
    } catch (e) {
      return false;
    }

    return true;
  }

  Future<bool> joinRoom(RoomId roomID) async {
    var userID = UserId()..id = auth.user.id;
    var joinRequest = JoinRequest()
      ..roomId = roomID
      ..userId = userID;
    await stub.joinRoom(joinRequest);
    return true;
  }

  void OnChatEvent(Event e) {
    if (e.hasMsg()) {
      if (e.msg.roomId == null) {
        return;
      }

      var currMsgs = messageStream.value;

      currMsgs[e.msg.roomId]?.add(e.msg);

      messageStream.add(currMsgs);
    }
  }

  void getAllRooms() async {}

  Future<List<RoomId>> getJoinedRooms() async {
    var userID = UserId()..id = auth.user.id;
    var rooms = await stub.getRoomsByUser(userID);
    print(rooms);
    return rooms.rooms;
  }

  Future<List<Message>> getLastMessagesByRoom(RoomId roomID, int count) async {
    var msgs = await stub.getLastMessagesByRoom(LastMessagesRequest()
      ..roomId = roomID
      ..count = count);

    if (msgs.messages.length > messageStream.value[roomID].length) {
      var currMsgs = messageStream.value;

      currMsgs[roomID] = msgs.messages;

      messageStream.add(currMsgs);
    }

    return msgs.messages;
  }

  void dispose() async {
    await channel.shutdown();
  }
}
