import 'package:rxdart/rxdart.dart';
import 'package:proto/api/common.pb.dart';
import 'package:proto/api/monster.pb.dart';

class MonsterServiceMock {
  BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>> currUserMons;
  BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>> currAllMons;
  Stream<MonsterEvent> _monEventStream;

  List<Monster> _createMonsters() {
    var monsters = [];

    for (var i = 0; i < 5; i++) {
      var body = BodyValues()
        ..maxSaturation = 100
        ..remainingSaturation = 10.0 * i
        ..mass = 10;

      var battleValues = BattleValues()
        ..attack = 10
        ..defense = 10;

      var mon = Monster()
        ..id = i
        ..bodyValues = body
        ..battleValues = battleValues;

      monsters.add(mon);
    }
    return monsters;
  }

  MonsterServiceMock() {
    var monsterMap = {};
    for (var m in _createMonsters()) {
      monsterMap.putIfAbsent(
          MonId()..id = m.id, () => BehaviorSubject.seeded(m));
    }

    currUserMons = BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>>.seeded(
        monsterMap);
    currAllMons = BehaviorSubject<Map<MonId, BehaviorSubject<Monster>>>.seeded(
        monsterMap);
  }
  Stream<MonsterEvent> get monEventStream => _monEventStream;
  void addMon(Monster mon) {
    throw UnimplementedError();
  }

  Future<Monster> GetMonByID(MonId id) async {
    return currAllMons.value[id].value;
  }

  Future<List<Monster>> GetMonByUserID(UserId id) async {
    var result = [];

    for (var mon in currAllMons.value.values) {
      if (mon.value.playerID == id.id) {
        result.add(mon.value);
      }

      return result;
    }
    return result;
  }

  ValueStream<Monster> getMonStreamByID(MonId id) {
    var mon = currAllMons.value.putIfAbsent(id, () {
      GetMonByID(id);
      return BehaviorSubject<Monster>.seeded(null);
    });

    return mon;
  }

  ValueStream<Map<MonId, ValueStream<Monster>>> getLoggedInMonstersStream() {
    return currUserMons;
  }

  void updateMon(Monster mon) {}

  void deleteMon(MonId id) {}
}
