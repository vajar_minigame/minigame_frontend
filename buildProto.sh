protoc  --dart_out=lib/ /usr/local/include/google/protobuf/timestamp.proto

protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/monster.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/user.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/controller.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/eventStream.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/auth.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/common.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/inventory.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/interactions.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/quest.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/chat.proto
protoc  -I=proto/  --dart_out=grpc:lib/api/ proto/battle.proto

